import logo from "./logo.svg";
import "./App.css";
import Header from "./BaiTapThucHanhLayout/Header";
import Body from "./BaiTapThucHanhLayout/Body";
import Footer from "./BaiTapThucHanhLayout/Footer";
import "./BaiTapThucHanhLayout/Styles.css";
function App() {
  return (
    <div className="App">
      <Header />
      <Body />
      <Footer />
    </div>
  );
}

export default App;
